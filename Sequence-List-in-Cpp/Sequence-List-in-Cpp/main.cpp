//
//  main.cpp
//  Sequence-List-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <iostream>
#include "SequenceList.hpp"
#include "Coordinate.hpp"

using namespace std;

void testIntSqList();
void testObjectSqList();

int main(int argc, const char * argv[]) {
    cout << "testIntSqList\n";
    testIntSqList();
    
    cout << "testObjectSqList\n";
    testObjectSqList();
    return 0;
}

void testIntSqList() {
    SequenceList<int> *sqList = new SequenceList<int>(5);
    
    sqList->insertElem(0, 1);
    sqList->insertElem(1, 2);
    sqList->insertElem(2, 3);
    sqList->insertElem(3, 4);
    sqList->insertElem(4, 5);
    
    sqList->listTraverse();
    
    int e = 0;
    sqList->deleteElem(1, &e);
    cout << "e: " << e << endl;
    
    sqList->listTraverse();
    
    sqList->getElem(2, &e);
    e = sqList->locateElem(e);
    cout << "e: " << e << endl;
    
    sqList->clearList();
    
    sqList->listTraverse();
    
    delete sqList;
    sqList = NULL;
}

void testObjectSqList() {
    SequenceList<Coordinate> *sqList = new SequenceList<Coordinate>(5);
    
    sqList->insertElem(0, Coordinate(1, 1));
    sqList->insertElem(1, Coordinate(2, 2));
    sqList->insertElem(2, Coordinate(3, 3));
    sqList->insertElem(3, Coordinate(4, 4));
    sqList->insertElem(4, Coordinate(5, 5));
    
    sqList->listTraverse();
    
    Coordinate *e = new Coordinate(0, 0);
    sqList->deleteElem(1, e);
    cout << "e: " << *e << endl;
    
    sqList->listTraverse();
    
    sqList->getElem(2, e);
    int n = sqList->locateElem(*e);
    cout << "n: " << n << endl;
    
    sqList->clearList();
    
    sqList->listTraverse();
    
    delete sqList;
    sqList = NULL;
}
