//
//  SequenceList.hpp
//  Sequence-List-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef SequenceList_hpp
#define SequenceList_hpp

#include <stdio.h>
#include <iostream>

using namespace std;

template <typename T>
class SequenceList {
    
public:
    SequenceList(int size);
    ~SequenceList();
    void clearList();
    bool isEmpty();
    bool isFull();
    int listLen();
    bool getElem(int i, T *e);
    int locateElem(T e);
    bool priorElem(T curElem, T *preElem);
    bool nextElem(T curElem, T *nextElem);
    bool insertElem(int i, T elem);
    bool deleteElem(int i, T *elem);
    void listTraverse();
    
    
private:
    T *m_pList;
    int m_iSize;
    int m_iLen;
};

template <typename T>
SequenceList<T>::SequenceList(int size) {
    m_iSize = size;
    m_pList = new T[m_iSize];
    
    clearList();
}

template <typename T>
SequenceList<T>::~SequenceList() {
    delete m_pList;
    m_pList = NULL;
}

template <typename T>
void SequenceList<T>::clearList() {
    m_iLen = 0;
}

template <typename T>
bool SequenceList<T>::isEmpty() {
    return m_iLen == 0;
}

template <typename T>
bool SequenceList<T>::isFull() {
    return m_iLen == m_iSize;
}

template <typename T>
int SequenceList<T>::listLen() {
    return m_iLen;
}

template <typename T>
bool SequenceList<T>::getElem(int i, T *e) {
    if (i < 0 || i >= m_iLen) {
        return false;
    }
    
    *e = m_pList[i];
    return true;
}

template <typename T>
int SequenceList<T>::locateElem(T e) {
    for (int i = 0; i < m_iLen; i ++) {
        if (m_pList[i] == e) {
            return i;
        }
    }
    return -1;
}

template <typename T>
bool SequenceList<T>::priorElem(T curElem, T *preElem) {
    int t = locateElem(curElem);
    if (t == -1 || t == 0) {
        return false;
    }
    
    *preElem = m_pList[t - 1];
    return true;
}

template <typename T>
bool SequenceList<T>::nextElem(T curElem, T *nextElem) {
    int t = locateElem(curElem);
    if (t == -1 || t == m_iLen - 1) {
        return false;
    }
    
    *nextElem = m_pList[t + 1];
    return true;
}

template <typename T>
bool SequenceList<T>::insertElem(int i, T elem) {
    if (i < 0 || i > m_iLen || isFull()) {
        return false;
    }
    
    for (int j = m_iLen - 1; j >= i; j --) {
        m_pList[j + 1] = m_pList[j];
    }
    m_pList[i] = elem;
    m_iLen += 1;
    return true;
}

template <typename T>
bool SequenceList<T>::deleteElem(int i, T *elem) {
    if (i < 0 || i > m_iLen || isEmpty()) {
        return false;
    }
    
    m_pList[i] = *elem;
    
    for (int j = i; j < m_iLen - 1; j ++) {
        m_pList[j] = m_pList[j + 1];
    }
    m_iLen -= 1;
    return true;
}

template <typename T>
void SequenceList<T>::listTraverse() {
    for (int i = 0; i < m_iLen; i ++) {
        cout << m_pList[i] << " ";
    }
    cout << endl;
}

#endif /* SequenceList_hpp */
